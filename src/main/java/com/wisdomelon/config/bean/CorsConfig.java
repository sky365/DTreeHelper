package com.wisdomelon.config.bean;

import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @author wisdomelon
 * @date 2020/2/27 0027
 * @project services
 * @jdk 1.8
 */
@Component
@ConfigurationProperties(prefix = "cors")
@Data
public class CorsConfig {

    @Value("#{'${cors.allowOrigins}'.split(',')}")
    private List<String> allowOrigins;

}
